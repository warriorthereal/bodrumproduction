const Sequelize = require("sequelize");
module.exports=function(sequelize, DataTypes){ 
  return Calisan = sequelize.define("calisan", {

    username : {
        type : Sequelize.STRING,
        require : true,
        validate : {

        },
        allowNull : false
    },
    email : {
        type : Sequelize.STRING,
        require : false,
        validate : {

        },
    },
    password : {
        type : Sequelize.STRING,
        require : true,

    },
    device_id : {
        type : Sequelize.STRING,
        require : false,
        allowNull : true,
        validate : {
        }
    },
    user_key : {
        type : Sequelize.STRING,
        require : true,
        allowNull : true,
        validate : {
        }
    },
  },  
 ) 
}
