const Sequelize = require("sequelize");
module.exports=function(sequelize, DataTypes){ 
  return Bildirim = sequelize.define("bildirim", {

    status :  {
        type : Sequelize.INTEGER,
        defaultValue : 0,
        allowNull : false
    },
    content : {
        type : Sequelize.STRING
    }
  },
)
}

