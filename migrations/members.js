const Sequelize = require("sequelize");
module.exports=function(sequelize, DataTypes){ 
  return Member = sequelize.define("members", {

    username : {
        type : Sequelize.STRING,
        require : true,
        unique : true,
        allowNull : false
    },
    password : {
        type : Sequelize.STRING,
        require : true,

    },
    admin_key : {
      type : Sequelize.STRING,
      require : true
    }
  }) 
    
}
