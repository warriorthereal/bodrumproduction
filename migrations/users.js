const Sequelize = require("sequelize");
module.exports=function(sequelize, DataTypes){ 
  return Users = sequelize.define("users", {

    
    username : {
        type : Sequelize.STRING,
        require : true,
        validate : {

        },
        allowNull : false
    },

    email : {
        type : Sequelize.STRING,
        require : true,
        validate : {

        },
    },
    password : {
        type : Sequelize.STRING,
        require : true,

    },
    phone : {
        type : Sequelize.STRING,
        require : true,
        validate : {

        },
    },
    device_id : {
        type : Sequelize.STRING,
        require : false,
        allowNull : true,
        validate : {
        }
    },
    user_key : {
        type : Sequelize.STRING,
        require : true,
        allowNull : true,
        validate : {
        }
    },
    

    
    
  },

  
 ) 
  

}
