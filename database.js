const Sequelize = require('sequelize');

const departman_firma_model = require('./migrations/departman_firma')
const userModel = require('./migrations/users');
const firmaModel = require('./migrations/firma');
const memberModel = require('./migrations/members')
const departmanModel = require('./migrations/departman')
const bildirimModel = require('./migrations/bildirimler')
const calisanModel = require('./migrations/calisan')
const sequelize = new Sequelize('bodrum','root','afoafoafo1A.',{
    host : 'localhost',
    dialect : 'mysql'

})

// const sequelize = new Sequelize('bodrum','root','afoafoafo1A.',{
//     host : 'localhost',
//     dialect : 'mysql'

// })

sequelize.sync().then(() => {

  console.log("Veritabanina Baglanildi.")

})
const departman_firma = departman_firma_model(sequelize,Sequelize)
const departman = departmanModel(sequelize,Sequelize)
const users = userModel(sequelize,Sequelize)
const firma = firmaModel(sequelize,Sequelize)
const members = memberModel(sequelize,Sequelize)
const bildirim = bildirimModel(sequelize,Sequelize)
const calisan = calisanModel(sequelize,Sequelize);


firma.hasMany(users,  {foreignKey: 'firma_id'});
users.belongsTo(firma , {foreignKey: 'firma_id' });
firma.hasMany(departman, {foreignKey : 'firma_id'});
departman.belongsTo(firma, {foreignKey : 'firma_id'});
firma.belongsTo(members ,{ targetKey : 'id' , foreignKey : 'admin' })
calisan.belongsTo(departman, {targetKey : 'id', foreignKey : 'departman_id'})
departman_firma.belongsTo(departman, {foreignKey : 'departman_id'})
departman_firma.belongsTo(firma, {foreignKey : "firma_id"})
bildirim.belongsTo(departman, {foreignKey : 'departman_id'})
bildirim.belongsTo(users, {foreignKey : 'user_id'})
module.exports = {
  firma,
  users,
  members,
  departman,
  bildirim,
  sequelize,
  calisan,
  departman_firma
}
