var db = require('./db'),
    sequelize = db.sequelize,
    Sequelize = db.Sequelize;

var feed = sequelize.define('feeds', {
    subscriber_id: Sequelize.INTEGER,
    activity_id: Sequelize.INTEGER
},
{
    tableName: 'feeds',
    freezeTableName: true
});