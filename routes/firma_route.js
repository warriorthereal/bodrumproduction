const express = require('express');
const router = express.Router();
const oneSignal = require('onesignal-node');

const {users, firma,departman_firma, departman,calisan,bildirim, sequelize} = require('../database');
const randomKey = require('random-key');

router.get('/', (req, res) => {

    res.send('firmalar')
    
});


router.post('/firma_kaydet', (req,res) => {

    const param_title = req.body.title;
    const param_foto = req.body.foto;
    const param_user = req.body.user_id;
    const param_mekan_id = randomKey.generateBase30(5)

    firma.findOne({
        where : {
            title : param_title
        }
    })
    .then((u) => {
        if(u) {
           // throw Error("TITLE ALINMIS")
           console.log(u)
           res.json({
               status : 'error',
               message : 'Kayit zaten var '
           })
          
    }else{ 


            
    firma.create({
        title : param_title,
        mekan_foto  : param_foto,
        user_id : param_user,
        mekan_id : param_mekan_id,
        
    }).then((row) => {
         res.json({
             status : "success",
             message : row
         });
    })
    .catch((err) => {
        console.log(err)
        res.json({
            status : "error",
            message : err.Violation
        })
    }).then((row) => {
        console.log("OLUSTURULAN" + row)
    })}


    })
   
   
    
    })


    // KULLANICININ FIRMASINI CEKME ~ YENI
router.post('/get_user_firma', (req,res) => {

    const params_user_key = req.body.user_key
    
    users.findOne({
        where :{
            user_key : params_user_key
        }
    }).then((user) => {
        console.log(user.firma_id)
        const user_firma = user.firma_id

        firma.findAll({
            where : {
                id : user_firma
            },
            attributes : ["title","mekan_foto", "id"]
        })
        .then((firmalar) => {
             res.json({
                 status : 'success',
                 firmalar : firmalar
             });
        }).catch((err) => {
             res.json({
                 status : 'error',
                 message : err
             });
        })
    })
})



// For User
// FIRMANIN DEPARTMANLARINI CEKME ~ YENI
router.post('/get_firma_departman', (req, res) => {

        const _firma_id = req.body.firma_id

        departman_firma.findOne({
            where : {
                firma_id : _firma_id
            }
          }).then((foundFirma) => {
            
            console.log("TEST",foundFirma)
            
            departman.findAll({
                where : {
                    firma_id : foundFirma.firma_id
                },
                attributes : ["id", "title"]

            }).then((departmanlar) => {
                res.json({
                    status : "success",
                    departmanlar : departmanlar
                })
            })
        })
});

// For Panel
// FIRMANIN CALISANLARINI CEKME (ADMIN PANEL ICIN) ~ YENI
router.get('/get_departman_calisan', (req, res) => {
    
    const _departman_id = req.body._departman_id

    calisan.findAll({
        where : {
            departman_id : _departman_id
        },
        attributes : ["username", "phone", "email"]
    })
    .then((calisanlar) => {
        res.json({
            status : "success",
            calisanlar : calisanlar
        })
    })
    .catch((err) => {
         res.json({
             status : "error",
             message : err
         });
    })
});

router.post('/set_player_id', (req,res) => {
    const params_player_id = req.body.player_id
    const params_user_key = req.body.user_key

    calisan.findOne({
      where : {
        user_key : params_user_key
      }
    }).then((calisan) => {
        calisan.update({
          device_id : params_player_id
        })

    }).then(() => {
       res.json({
         status : "success",
         message : "PLAYER ID EKLENDI"
       });
    }).catch((err) => {
      console.log(err)
      res.json({
        status : "error",
        
      })
    })
  })

    router.post('/calisan_ekle', (req, res) => {

        const _username = req.body.username;
        const _password = req.body.password;
        const _email = req.body.email;
        const _departman_id = req.body.departman
       
                calisan.create({
                    username : _username,
                    password : _password,
                    email : _email,
                    departman_id : _departman_id,
                    user_key : randomKey.generate(10)
                }).then((calisan) => {
                    res.json({
                        status : 'success',
                        calisan : calisan.username
                    })
                })
         


  });   

  router.post('/departman_ekle', (req, res) => {
    const _title = req.body.title
    const _firma_id = req.body.firma_id

    departman.create({
        title : _title,
        firma_id : _firma_id
    }).then((_departman) => {
        departman_firma.create({
            departman_id : _departman.id,
            firma_id : _firma_id
        })

        res.json({
            status : 'success',
            departman : _departman
        })
    }).catch((err) => {
        res.json({
            status : 'error',
            message : err
        })
    })
  });

  router.post('/departman_bildirimleri', (req, res) => {
        const _user_key = req.body.user_key

        calisan.findOne({
            where : {
                user_key : _user_key
            }
        }).then((calisan) => {
            
            sequelize.query(`select bildirims.*, users.username from bildirims inner join users on bildirims.user_id = users.id where bildirims.status = 0 AND departman_id = ${calisan.departman_id}`)
            .then((bildirimler) => {
            // bildirim.findAll({
            //     where : {
            //         departman_id : calisan.departman_id,
            //         status : 0
            //     }
            // }).then((bildirimler) => {
                 res.json({
                     status : 'success',
                     bildirimler : bildirimler
                 });
            })
        })
  });


  router.post('/geriBildirim', (req, res) => {
        const _bildirim_id = req.body.bildirim_id
        // USER KEY CALISANIN CALISAN ADI CEKILECEK
        const _user_key = req.body.user_key
        bildirim.findOne({
            where : {
                id : _bildirim_id
            }
        }).then((bildir) => {

            bildir.update({
                status : "1"
            })
            console.log("BILL", bildir.user_id)
            const bildirim_user_id = bildir.user_id



            users.findOne({
                where : {
                    id : bildirim_user_id
                }
            }).then((userW) => {
                
                calisan.findOne({
                    where : {
                        user_key : _user_key
                    }
                }).then((calisan) => {
                    // BILDIRIM HAZIRLA GONDER

                    const one_client = new oneSignal.Client({
                        userAuthKey : 'NzMwZWE5ZDAtZmE3Yi00ZmM1LTg1MTAtMzE2MWYwNzQ5YmYw',
                        app : {
                           appAuthKey : 'YTUwN2IzNjUtZDgwYy00YzJjLWI1ZjYtNzNmMmFkZDdkMjJk',
                           appId : 'd88f4045-bc1f-4806-94f2-2d64b2ee5574'
                      }
                      })
                    
                      const notification = new oneSignal.Notification({
                        headings : {
                          en : `Isteginizle Ilgileniliyor.`
                        },
                    
                        contents : {
                          en : `Isteginizle ${calisan.username} ilgileniyor.`
                        },
                        include_player_ids : [userW.device_id]
                      })
                    
                      one_client.sendNotification(notification)
                      
                      .then((response) => {
                        console.log(response)
                    
                      
                      })
                      .then(() => {
                    
                        res.json({
                          status : "success",
                          message : "MESSAGE OK"
                        })
                    
                      })
                      .catch((err) => {
                        res.json({
                          status : "error",
                          message : "MESSAGE ERROR"
                        })
                       })
                })
                
                
            })
        })
  });


module.exports = router