
const express = require('express');
const router = express.Router();
const axios = require('axios');
const {users, firma, departman, sequelize, calisan, members} = require('../database');
const randomKey = require('random-key');
const userss = require('../controllers/users_controller')
const oneSignal = require('onesignal-node');
const pluck = require('arr-pluck');
/* GET users listing. */

router.get('/', (req,res) => {
  users.findAll({
    attributes : ["username"],

    include : {
      model : departman,
      as : "departman",
      attributes : ['title'],
      raw : true
    }
  })
 
  .then((user) => {
    res.json({
      user : user
    });
 })
 
})

router.get('/test', (req, res) => {
  sequelize.query("select users.username, departmans.title from users inner join departmans on users.departman_id  = departmans.id", { type : sequelize.QueryTypes.SELECT})
  .then((users) => {
    res.json({
      users : users
    })
  })
});


  router.post('/firmanin_calisanlari', (req,res) => {

      const param_firma_id = req.body.firma_id;

      users.findAll({
        where : {
          user_firma : param_firma_id
        }
      }).then((users) => {
        
          res.json({
            status : 'success',
            message : users
          })
      })


  })

  router.post('/login', (req, res) => {

    const param_username = req.body.username;
    const param_password = req.body.password;

    users.findOne({
      where : {
        username : param_username,
        password : param_password
      }
    })
    .then((user) => {

      console.log(user)
      res.json({
        status : 'success',
        username : user.username,
        key : user.user_key
      })

    }).catch((err) => {
      res.json({
        status : 'error',
        message : err
      })
    })
  });

  router.post('/login_calisan', (req, res) => {

    const param_username = req.body.username;
    const param_password = req.body.password;

    calisan.findOne({
      where : {
        username : param_username,
        password : param_password
      }
    })
    .then((calisan) => {

      res.json({
        status : 'success',
        username : calisan.username,
        key : calisan.user_key
      })

    }).catch((err) => {
      console.log("ERR", err)
      res.json({
        status : 'error',
        message : err
        
      })
    })
  });

  router.post('/uye_sil', (req, res) => {
      const params_username = req.body.username;

      users.destroy({
        where : {
          username : params_username
        }
      }).then(() => {
        res.json({
          message : "success"
        })
      })
      .catch(() => {
        res.json({
          message : 'error'
        })
      })
  });
 
  router.post('/uye_yarat', (req, res) => {
    const param_username = req.body.username;
    const param_email = req.body.email;
    const param_password = req.body.password;
    const param_phone = req.body.phone;
    const param_device_id = req.body.device_id;
    const param_user_key = randomKey.generateBase30(30);
    const _user = req.body.user_key

    members.findOne({
      where : {
          admin_key : _user
      }
  }).then((admin) => {
      console.log(admin.id)
      firma.findOne({
          where : {
          admin : admin.id
          }
        })
      .then((firma) => {
        users.create({
          username : param_username,
          email : param_email,
          password : param_password,
          phone : param_phone,
          device_id : param_device_id,
          user_key : param_user_key,
          firma_id : firma.id
        }).then((row) => {
    
          const iletiusername = "5310202212";
          const iletipassword = "2489914";
    
          axios.get(`https://api.iletimerkezi.com/v1/send-sms/get/?username=${iletiusername}&password=${iletipassword}&text=Muhattapp%20Kullanici%20Adiniz%20${row.username}%20Sifreniz%20${row.password}%20Sistem%20Kaydiniz%20${firma.title}%20Tarafindan%20alinmistir.&receipents=${row.phone}&sender=Asyonsis`)
          .then((response) =>{
    
            console.log(response)
          }).catch((error) => {
            console.log(error)
          })
    
          res.json({
            status : 'success',
            message : row
          })
        }).catch((err) => {
          res.json({
            status : 'error',
            message : err
          })
        })
        })
  })

      console.log(departman)
      
  });


  router.post('/set_player_id', (req,res) => {
    const params_player_id = req.body.player_id
    const params_user_key = req.body.user_key

    users.findOne({
      where : {
        user_key : params_user_key
      }
    }).then((user) => {
        user.update({
          device_id : params_player_id
        })

    }).then(() => {
       res.json({
         status : "success",
         message : "PLAYER ID EKLENDI"
       });
    }).catch((err) => {
      console.log(err)
      res.json({
        status : "error",
        
      })
    })
  })
  
module.exports = router;


