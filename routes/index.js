var express = require('express');
var router = express.Router();
const {users, bildirim, sequelize, calisan} = require('../database');
const oneSignal = require('onesignal-node');
var pluck = require('arr-pluck');
const randomKey = require('random-key')


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


//NEW
router.post('/bildirimlerim', (req,res) => {
  const _user_key = req.body.user_key
  users.findOne({
      where : {
        user_key : _user_key
      }
    }).then((user) => {
      bildirim.findAll({
        where : {
          user_id : user.id
        },
        attributes : ['content', 'status']
      }).then((bildirimler) => {
         res.json({
           status : 'success',
           bildirimler : bildirimler
         });
      })
    })
  })
  












router.post('/feedback', (req,res) => {
  const _bildirimid = req.body.bildirim_id

  bildirim.findOne({
    where : {
      bildirim_id : _bildirimid
    }
  }).then((bildirim) => {

    var calisan_username
    const calisan = bildirim.calisan

    users.findOne({
      where : {
        device_id : calisan
      }
    }).then((calisan_user) => {
      calisan_username = calisan_user.username
      console.log("CALISAN" + calisan_username)


      bildirim.update({
        status : "1"
      })
  
    const one_client = new oneSignal.Client({
      userAuthKey : 'NzMwZWE5ZDAtZmE3Yi00ZmM1LTg1MTAtMzE2MWYwNzQ5YmYw',
      app : {
         appAuthKey : 'NTMzZTE0MjUtMzU2NC00ZDUxLWE0ODItNDA2Mzc0ODY2NDQ5',
         appId : 'c4be9b15-19b0-48b1-9948-4d1813072d5f'
    }
    })
  
    const notification = new oneSignal.Notification({
      headings : {
        en : `Isteginizle Ilgileniliyor.`
      },
  
      contents : {
        en : `Isteginizle ${calisan_username} ilgileniyor.`
      },
      include_player_ids : [bildirim.musteri]
    })
    console.log("BILDIRIM " + bildirim.musteri)
  
    one_client.sendNotification(notification)
    
    .then((response) => {
      console.log(response)
  
    
    })
    .then(() => {
  
      res.json({
        status : "success",
        message : "MESSAGE OK"
      })
  
    })
    .catch((err) => {
      res.json({
        status : "error",
        message : "MESSAGE ERROR"
      })
     })
  
    })
    })
})

// BILDIRIM GONDERIP KAYIT EDIYORUZ ~ YENI
router.post('/bildirim_gonder', (req, res) => {

  const _content = req.body.content;
  const _departman_id = req.body.departman_id;
  const _user_key = req.body.user_key


  users.findOne({
    where : {
      user_key : _user_key
    },
    raw : true
  }).then((user) => {
    const _user_id = user.id;

    bildirim.create({
      user_id : _user_id,
      content : _content,
      departman_id : _departman_id
    }).then((bildirimW) => {
      // CALISANLARA BILDIRIM GONDERME
        calisan.findAll({
          where : {
            departman_id : _departman_id
          },
          attributes : ["device_id"],
          raw : true
        }).then((calisanlar) => {
         

          const ids =  pluck(calisanlar, "device_id")

          console.log("IDD", ids)
            const one_client = new oneSignal.Client({
                  userAuthKey : 'NzMwZWE5ZDAtZmE3Yi00ZmM1LTg1MTAtMzE2MWYwNzQ5YmYw',
                  app : {
                    appAuthKey : 'YTUwN2IzNjUtZDgwYy00YzJjLWI1ZjYtNzNmMmFkZDdkMjJk',
                    appId : 'd88f4045-bc1f-4806-94f2-2d64b2ee5574'
                }
              })


                  const notification = new oneSignal.Notification({
                    headings : {
                      en : `${user.username} Adlı Müşteri cagiriyor.`
                    },

                    contents : {
                      en : _content
                    },
                    include_player_ids: ids
                  })


                  one_client.sendNotification(notification)
                  .then((response) => {
                    console.log(response)

                  
                  })
                  .then(() => {

                    res.json({
                      status : "success",
                      message : user.username
                    })

                  })
                  .catch((err) => {
                    res.json({
                      status : "error",
                      message : err
                    })
                  })
        })
    })
  })
});


router.post('/bildirim', (req,res) => {

  const params_content = req.body.content;
  const params_firma_id = req.body.firma_id;
  const params_user_type = req.body.user_type;
  const params_user_id = req.body.user_key;
  const params_device_id = req.body.device_id

  var sendUser = "ASD"

  console.log("USER ID " + params_user_id)



  users.findOne({
    where :  {
      user_key : params_user_id
    }
  })
  .then((user) => {
    sendUser = user.username
  })


  users.findAll({
    where : {
      user_firma : params_firma_id,
      user_type : params_user_type
    },
    attributes : ['device_id'],
    raw : true
  })

  .then((users) => {
     const ids =  pluck(users, "device_id")

     const names = pluck(users, "username")

     
     bildirim.create({
//
      calisan : ids.toString(),
      musteri : params_device_id,
      bildirim_id : randomKey.generateDigits(6),
      musteriler : names.toString(),
      content : params_content
  })
 

  const one_client = new oneSignal.Client({
    userAuthKey : 'NzMwZWE5ZDAtZmE3Yi00ZmM1LTg1MTAtMzE2MWYwNzQ5YmYw',
    app : {
       appAuthKey : 'NTMzZTE0MjUtMzU2NC00ZDUxLWE0ODItNDA2Mzc0ODY2NDQ5',
       appId : 'c4be9b15-19b0-48b1-9948-4d1813072d5f'
  }
  })

  const notification = new oneSignal.Notification({
    headings : {
      en : `${params_user_type} için ${sendUser} cagiriyor.`
    },

    contents : {
      en : params_content
    },
    include_player_ids: ids
  })

  

  one_client.sendNotification(notification)
  .then((response) => {
    console.log(response)

  
  })
  .then(() => {

    res.json({
      status : "success",
      message : user.username
    })

  })
  .catch((err) => {
    res.json({
      status : "error",
      message : err
    })
  })
  
  })
  // .then((ids) => {
  //   console.log("IDLER" + ids)


  // })
  
})



module.exports = router;


