var express = require('express');
var router = express.Router();
const {users, bildirim, sequelize, calisan, departman, firma, members,departman_firma} = require('../database');
const randomKey = require('random-key')
const multer = require('multer')
const fs = require('fs')
const path = require('path')
const pluck = require('arr-pluck')

const handleError = (err, res) => {
    res
      .status(500)
      .contentType("text/plain")
      .end("Oops! Something went wrong!");
  };

  const upload = multer({
    dest : "./uploads/"
  })

router.post('/firma_listesi', (req, res) => {
        const _id = req.body._id

        firma.findAll({
            where : {
                admin : _id
            }
        }).then((firmalar) => {
            res.json({
                status : 'Success',
                firmalar : firmalar
            })
        })
        .catch((err) => {
            res.json({
                status : 'error',
                error : err
            })
        })

});


router.post('/admin_yarat', (req,res) => {

    const _username = req.body.username;
    const _password = req.body.password;
    const _admin_key = randomKey.generateBase30(8)


    members.create({
        username : _username,
        password : _password,
        admin_key : _admin_key
    }).then((member) => {
        res.json({
            status : 'Success',
            member : member
        })
    })
    .catch((err) => {
        res.json({
            status : 'Error',
            Error : err
        })
    })
})

router.post('/admin_giris', (req, res) => {
    const _username = req.body.username;
    const _password = req.body.password;

    members.findOne({
        where : {
            username : _username,
            password : _password
        },

    }).then((member) => { 
        if(member){
            res.json({
                status : 'success',
                key : member.admin_key
            })
        }else {
            res.json({
                status : 'error',
                message : member
            })
        }
    }).catch((err) => {
        res.json({
            status: 'error',
            message : "ASD"
        })
    })
});


router.post('/firma_yarat',upload.single("file"), (req, res) => {

    const tempPath = req.file.path;
    const randomed = randomKey.generateDigits(10)
    const targetPath = path.join(__dirname, `../public/images/${randomed}.png`);


    const _title = req.body.title;
    const _id = req.body.id;

    
    firma.create({
        title : _title,
        mekan_foto : `https://muhattapp.8black.co/images/${randomed}.png`,
        mekan_id : randomKey.generateBase30(6),
        admin : _id
    })
    .then((firma) => {
        
        if (path.extname(req.file.originalname).toLowerCase() === ".png") {
            fs.rename(tempPath, targetPath, err => {
                console.log(err)
             if (err) return handleError(err, res);
          
            });
          } else {
            fs.unlink(tempPath, err => {
                console.log(err)

              if (err) return handleError(err, res);
            });
          }
        res.json({
            status : 'Success',
            firma : firma
        })
    })
    .catch((err) => {
        res.json({
            status : 'Error',
            Error : err
        })
    })
});

router.post('/asd', (req, res) => {
    departman.findAll({
        where : {
            id : 1
        }
    })
    .then((firma) => {
        res.send(firma)
    })
});

router.post('/departman_listesi', (req,res) => {

    const _user = req.body.user_key

    members.findOne({
        where : {
            admin_key : _user
        }
    }).then((admin) => {
        console.log("ADDD",admin.id)
        firma.findOne({
            where : {
            admin : admin.id
            }
          })
        .then((firma) => {
            console.log(firma.id)
            departman_firma.findAll({
                where : {
                    firma_id : firma.id
                },
                attributes : ['id']
            }).then((ids) => {
               const asd =  pluck(ids,'id')
               console.log("ISSS", asd)
               departman.findAll({
                where : {
                    id : asd
                }
            }).then((departmans) => {
                console.log("DEPP",departmans)
                res.send(departmans)
            })
            })
        })

    })
    .catch((err) => {
        console.log("ERRR", err)
    })
})

router.post('/departman_ekle', (req, res) => {

    const _user = req.body.user_key
    const _title = req.body.title;

    members.findOne({
        where : {
            admin_key : _user
        }
    }).then((admin) => {
        console.log("ADDD",admin.id)
        firma.findOne({
            where : {
            admin : admin.id
            }
          })
        .then((firma) => {
            console.log("FIRMAA", firma)
           const firma_id = firma.id
            departman.create({
                title : _title,
                firma_id : firma_id
            }).then((departman) => { 
                departman_firma.create({
                    departman_id : departman.id,
                    firma_id : firma_id
                })

                res.json({departman})
            })
          })
    })
}); 

router.post('/calisanlar', (req, res) => {

    const _departman_id = req.body.departmanid

    calisan.findAll({
        where : {
            departman_id : _departman_id
        }
    })
    .then((calisanlar) => {
        res.json({
            status : 'Success',
            Calisanlar : calisanlar
        })
    })

    .catch((err) => {
        res.json({
            status : "Error",
            Error : err
        })
    })
});

module.exports = router;
